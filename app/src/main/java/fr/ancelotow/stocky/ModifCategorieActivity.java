package fr.ancelotow.stocky;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.technical.Session;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ModifCategorieActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    final static int SELECT_PICTURE = 1;
    Categorie categorie;
    TextView tvError;
    EditText etNom;
    ImageView ivImg;
    Button btnImg;
    Button btnModif;
    Bitmap bitmap = null;
    String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_categorie);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        btnImg = (Button) findViewById(R.id.btnImg);
        btnModif = (Button) findViewById(R.id.btnModif);
        ivImg = (ImageView) findViewById(R.id.ivImg);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        init(this);
        final long idCategorie = pack.getLong("idCategorie");
        CategorieDAO db = new CategorieDAO(this);
        db.ouvrir();
        categorie = db.getCategorie(idCategorie);
        db.fermer();
        setTitle(categorie.getNom().toUpperCase());
        etNom.setText(categorie.getNom());
        if(categorie.getImage().equals("aucune")){
            bitmap = BitmapFactory.decodeFile(categorie.getImage());
            ivImg.setImageBitmap(bitmap);
        }
        View.OnClickListener image = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                requestRead(intent);
            }
        };
        View.OnClickListener modifier = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("")){
                    tvError.setText("Le champ NOM doit être rempli.");
                }
                else{
                    if(bitmap != null){
                        saveImg();
                    }
                    categorie.setNom(etNom.getText().toString());
                    categorie.setStock(Session.getSession().getStock());
                    CategorieDAO db = new CategorieDAO(ModifCategorieActivity.this);
                    db.ouvrir();
                    db.modifCategorie(categorie);
                    db.fermer();
                    Toast.makeText(ModifCategorieActivity.this,
                            "Catégorie modifié avec succès"
                            , Toast.LENGTH_LONG).show();
                    Bundle pack = new Bundle();
                    pack.putLong("idCategorie", categorie.getId());
                    Intent i2 = new Intent(ModifCategorieActivity.this,
                            CategorieActivity.class);
                    i2.putExtras(pack);
                    startActivity(i2);
                }
            }
        };
        btnImg.setOnClickListener(image);
        btnModif.setOnClickListener(modifier);

    }

    public void requestRead(Intent intent) {
        if (ContextCompat.checkSelfPermission(this,
                READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
        }
    }

    public void requestWrite() {
        if (ContextCompat.checkSelfPermission(this,
                WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        } else {
            saveImg();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
            }
            else {
                Toast.makeText(ModifCategorieActivity.this,
                        "Permission refusé.", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        else if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveImg();
            }
            else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Bundle pack = new Bundle();
        pack.putLong("idCategorie", categorie.getId());
        Intent i2 = new Intent(ModifCategorieActivity.this,
                CategorieActivity.class);
        i2.putExtras(pack);
        startActivity(i2);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_PICTURE:
                    String path = getRealPathFromURI(this, data.getData());
                    Log.d("Choose Picture", path);
                    bitmap = BitmapFactory.decodeFile(path);
                    ivImg.setImageBitmap(bitmap);
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getRealPathFromURI(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    public void init(Context context) {
        path = context.getExternalFilesDir(null).getAbsolutePath();
    }

    public void saveImg(){
        try {
            FileOutputStream output = null;
            File file = new File(path,
                    etNom.getText().toString().replaceAll("\\s", "") + "_" +
                    Session.getSession().getStock().getNom().replaceAll("\\s", "") +
                    Session.getSession().getStock().getId() + ".png");
            output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            File dfile = new File(categorie.getImage());
            dfile.delete();
            categorie.setImage(file.getAbsolutePath());
            output.flush();
            output.close();
            MediaStore.Images.Media.insertImage(getContentResolver(),
                    file.getAbsolutePath(),file.getName(),file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
