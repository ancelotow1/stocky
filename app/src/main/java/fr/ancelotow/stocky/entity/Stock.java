package fr.ancelotow.stocky.entity;

public class Stock {

    private long id;
    private String nom;
    private double budget;
    private boolean util;

    public Stock(){
        super();
    }

    public Stock(long id, String nom, double budget) {
        super();
        this.id = id;
        this.nom = nom;
        this.budget = budget;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public boolean isUtil() {
        return util;
    }

    public void setUtil(boolean util) {
        this.util = util;
    }

    public void modifBudget(double montant){
        this.budget = this.budget - montant;
    }

    @Override
    public String toString(){
        return nom.toUpperCase();
    }

}
