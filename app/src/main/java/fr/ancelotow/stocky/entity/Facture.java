package fr.ancelotow.stocky.entity;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Facture {

    private long id;
    private LocalDate date;
    private double montant;
    private Stock stock;

    public Facture(){
        super();
    }

    public Facture(long id, LocalDate date, double montant, Stock stock) {
        super();
        this.id = id;
        this.date = date;
        this.montant = montant;
        this.stock = stock;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public String toString(){
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("d/MM/uuuu");
        return date.format(formatDate);
    }

}
