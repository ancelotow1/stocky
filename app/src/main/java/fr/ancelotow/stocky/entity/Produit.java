package fr.ancelotow.stocky.entity;

public class Produit {

    private String id;
    private String nom;
    private String image;
    private int quantite;
    private int alert;
    private double prix;
    private Vendeur vendeur;
    private Categorie categoerie;

    public Produit(){
        super();
    }

    public Produit(String id, String nom, String image, int quantite, int alert, double prix,
                   Vendeur vendeur, Categorie categoerie) {
        super();
        this.id = id;
        this.nom = nom;
        this.image = image;
        this.quantite = quantite;
        this.alert = alert;
        this.prix = prix;
        this.vendeur = vendeur;
        this.categoerie = categoerie;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public int getAlert() {
        return alert;
    }

    public void setAlert(int alert) {
        this.alert = alert;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Vendeur getVendeur() {
        return vendeur;
    }

    public void setVendeur(Vendeur vendeur) {
        this.vendeur = vendeur;
    }

    public Categorie getCategoerie() {
        return categoerie;
    }

    public void setCategoerie(Categorie categoerie) {
        this.categoerie = categoerie;
    }

    @Override
    public String toString(){
        return nom;
    }

}
