package fr.ancelotow.stocky.entity;

public class Vendeur {

    private long id;
    private String nom;
    private String tel;
    private String email;
    private String url;
    private Stock stock;

    public Vendeur(){
        super();
    }

    public Vendeur(long id, String nom, String tel, String email, String url, Stock stock) {
        this.id = id;
        this.nom = nom;
        this.tel = tel;
        this.email = email;
        this.url = url;
        this.stock = stock;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString(){
        return nom.toUpperCase();
    }

}
