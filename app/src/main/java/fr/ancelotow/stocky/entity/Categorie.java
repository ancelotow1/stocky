package fr.ancelotow.stocky.entity;

public class Categorie {

    private long id;
    private String nom;
    private String image;
    private Stock stock;

    public Categorie(){
        super();
    }

    public Categorie(long id, String nom, String image, Stock stock) {
        super();
        this.id = id;
        this.nom = nom;
        this.image = image;
        this.stock = stock;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString(){
        return nom;
    }

}
