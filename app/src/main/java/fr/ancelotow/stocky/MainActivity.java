package fr.ancelotow.stocky;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DecimalFormat;
import java.time.LocalDate;

import fr.ancelotow.stocky.database.FactureDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.entity.Facture;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.technical.Session;

import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView tvNomStock, tvBudget;
    Button btnAction;
    boolean action = false;
    final DecimalFormat df = new DecimalFormat("0.000");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnAction = (Button) findViewById(R.id.btnAction);
        tvBudget = (TextView) findViewById(R.id.tvBudget);
        tvBudget.setText("BUDGET : " +
                String.valueOf(df.format(Session.getSession().getStock().getBudget())) + " €");
        btnAction.setText("RETIRER");
        btnAction.setBackgroundResource(R.drawable.opt_button_red);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    new com.google.zxing.integration.android.IntentIntegrator(
                            MainActivity.this)
                            .setPrompt("Scannerz le produit (Alignez la barre rouge avec le code)")
                            .initiateScan();
                } else {
                    requestPermissions(new String[]{CAMERA}, 1);
                }
            }
        });
        View.OnClickListener actionner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!action){
                    action = true;
                    btnAction.setText("AJOUTER");
                    btnAction.setBackgroundResource(R.drawable.opt_btn);
                    Toast.makeText(MainActivity.this,
                            "Le scanner retirera des produit en scannant."
                            , Toast.LENGTH_LONG).show();
                }
                else{
                    action = false;
                    btnAction.setText("RETIRER");
                    btnAction.setBackgroundResource(R.drawable.opt_button_red);
                    Toast.makeText(MainActivity.this,
                            "Le scanner ajoutera des produit en scannant."
                            , Toast.LENGTH_LONG).show();
                }
            }
        };
        btnAction.setOnClickListener(actionner);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvNomStock = (TextView) headerView.findViewById(R.id.tvNomStock);
        tvNomStock.setText(Session.getSession().getStock().getNom());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navProduit) {
            Intent i = new Intent(MainActivity.this, ListProduitActivity.class);
            startActivity(i);
        } else if (id == R.id.navCategorie) {
            Intent i = new Intent(MainActivity.this, ListCategorieActivity.class);
            startActivity(i);
        } else if (id == R.id.navCourse) {
            Intent i = new Intent(MainActivity.this, CourseActivity.class);
            startActivity(i);
        } else if (id == R.id.navVendeur) {
            Intent i = new Intent(MainActivity.this, ListVendeurActivity.class);
            startActivity(i);
        } else if (id == R.id.navBudget) {
            Intent i = new Intent(MainActivity.this, BudgetActivity.class);
            startActivity(i);
        } else if (id == R.id.navParam) {
            Intent i = new Intent(MainActivity.this,
                    ParametreActivity.class);
            startActivity(i);
        } else if (id == R.id.navQuitter) {
            StockDAO db = new StockDAO(this);
            db.ouvrir();
            db.setStockUtil(Session.getSession().getStock());
            db.fermer();
            Session.fermer();
            Intent i = new Intent(MainActivity.this, ChoiceStockActivity.class);
            startActivity(i);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, intent);
        if(scanningResult != null){
            if(!action){
                String id = scanningResult.getContents();
                ProduitDAO db = new ProduitDAO(this);
                db.ouvrir();
                Produit produit = db.getProduit(id);
                if(produit == null){
                    Bundle pack = new Bundle();
                    pack.putString("idProduit", id);
                    Intent i2 = new Intent(MainActivity.this,
                            AddProduitActivity.class);
                    i2.putExtras(pack);
                    startActivity(i2);
                    db.fermer();
                }
                else{
                    Toast.makeText(MainActivity.this,
                            "Ajout " + produit.getNom() + " réussi."
                            , Toast.LENGTH_LONG).show();
                    produit.setQuantite(produit.getQuantite() + 1);
                    Session.getSession().getStock().setBudget(
                            Session.getSession().getStock().getBudget() - produit.getPrix()
                    );
                    tvBudget.setText("BUDGET : " +
                            String.valueOf(df.format(
                                    Session.getSession().getStock().getBudget()))  + " €");
                    db.modifProduit(produit);
                    db.fermer();
                    StockDAO dbStock = new StockDAO(this);
                    dbStock.ouvrir();
                    dbStock.setStockBudget(Session.getSession().getStock());
                    dbStock.fermer();
                    Facture facture = new Facture();
                    facture.setDate(LocalDate.now());
                    facture.setMontant(produit.getPrix());
                    facture.setStock(Session.getSession().getStock());
                    FactureDAO dbFac = new FactureDAO(this);
                    dbFac.ouvrir();
                    dbFac.addFacture(facture);
                    dbFac.fermer();
                }
            }
            else{
                String id = scanningResult.getContents();
                ProduitDAO db = new ProduitDAO(this);
                db.ouvrir();
                Produit produit = db.getProduit(id);
                if(produit == null){
                    Toast.makeText(MainActivity.this,
                            "ERREUR : Ce produit n'existe pas."
                            , Toast.LENGTH_LONG).show();
                    db.fermer();
                }
                else{
                    if(produit.getQuantite() == 0){
                        Toast.makeText(MainActivity.this,
                                "ERREUR : Ce produit est déjà en rupture de stock."
                                , Toast.LENGTH_LONG).show();
                        db.fermer();
                    }
                    else{
                        Toast.makeText(MainActivity.this,
                                "1 " + produit.getNom() + " retiré avec succès."
                                , Toast.LENGTH_LONG).show();
                        produit.setQuantite(produit.getQuantite() - 1);
                        db.modifProduit(produit);
                        db.fermer();
                    }
                }
            }
        }
        else{
            Toast.makeText(MainActivity.this,
                    "Aucune donnée trouvée."
                    , Toast.LENGTH_LONG).show();
        }
    }

}
