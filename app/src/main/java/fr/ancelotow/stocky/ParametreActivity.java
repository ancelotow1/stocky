package fr.ancelotow.stocky;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.FactureDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.technical.Session;

public class ParametreActivity extends AppCompatActivity {

    Button btnRename, btnBudget, btnSup;
    AlertDialog.Builder alertSup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametre);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(Session.getSession().getStock().getNom());
        btnRename = (Button) findViewById(R.id.btnRenommer);
        btnBudget = (Button) findViewById(R.id.btnBudget);
        btnSup = (Button) findViewById(R.id.btnSupprimer);
        alertSup = new AlertDialog.Builder(this);
        alertSup.setPositiveButton(R.string.btnOuiSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                CategorieDAO dbCat = new CategorieDAO(ParametreActivity.this);
                dbCat.ouvrir();
                List<Categorie> categories = dbCat.getCategorieStock(
                        Session.getSession().getStock().getId());
                dbCat.fermer();
                ProduitDAO dbProd = new ProduitDAO(ParametreActivity.this);
                dbProd.ouvrir();
                for(Categorie unC : categories){
                    dbProd.supProduitCat(unC.getId());
                    File dfile = new File(unC.getImage());
                    dfile.delete();
                }
                dbProd.fermer();
                dbCat.ouvrir();
                dbCat.supCategorieStock(Session.getSession().getStock().getId());
                dbCat.fermer();
                VendeurDAO dbVend = new VendeurDAO(ParametreActivity.this);
                dbVend.ouvrir();
                dbVend.supVendeurStock(Session.getSession().getStock().getId());
                dbVend.fermer();
                FactureDAO dbFac = new FactureDAO(ParametreActivity.this);
                dbFac.ouvrir();
                dbFac.supFactureStock(Session.getSession().getStock().getId());
                dbFac.fermer();
                StockDAO dbStock = new StockDAO(ParametreActivity.this);
                dbStock.ouvrir();
                dbStock.supStock(Session.getSession().getStock());
                dbStock.fermer();
                Toast.makeText(ParametreActivity.this,
                        "Stock supprimé avec succès."
                        , Toast.LENGTH_LONG).show();
                Session.fermer();
                Intent intentionEnvoyer = new Intent(ParametreActivity.this,
                        ChoiceStockActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        alertSup.setNegativeButton(R.string.btnNonSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertSup.setMessage(R.string.tvSupStock);
        View.OnClickListener renommer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ParametreActivity.this,
                        RenameActivity.class);
                startActivity(i);
            }
        };
        View.OnClickListener budgeter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ParametreActivity.this,
                        BudgetStockActivity.class);
                startActivity(i);
            }
        };
        View.OnClickListener supprimer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSup.show();
            }
        };
        btnRename.setOnClickListener(renommer);
        btnBudget.setOnClickListener(budgeter);
        btnSup.setOnClickListener(supprimer);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ParametreActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

}
