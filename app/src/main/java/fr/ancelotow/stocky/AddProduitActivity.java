package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class AddProduitActivity extends AppCompatActivity {

    TextView tvError;
    EditText etNom;
    EditText etPrix;
    EditText etQuantite;
    EditText etAlert;
    Spinner spCategorie;
    Spinner spVendeur;
    Button btnAjouter;
    List<Categorie> categories = new ArrayList<Categorie>();
    List<Vendeur> vendeurs = new ArrayList<Vendeur>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_produit);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final String idProduit = pack.getString("idProduit");
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        etQuantite = (EditText) findViewById(R.id.etQuantite);
        etAlert = (EditText) findViewById(R.id.etAlert);
        etPrix = (EditText) findViewById(R.id.etPrix);
        spCategorie = (Spinner) findViewById(R.id.spCategorie);
        spVendeur = (Spinner) findViewById(R.id.spVendeur);
        btnAjouter = (Button) findViewById(R.id.btnAjouter);
        CategorieDAO dbCat = new CategorieDAO(this);
        dbCat.ouvrir();
        categories = dbCat.getCategorieStock(Session.getSession().getStock().getId());
        dbCat.fermer();
        VendeurDAO dbVend = new VendeurDAO(this);
        dbVend.ouvrir();
        vendeurs = dbVend.getVendeurStock(Session.getSession().getStock().getId());
        dbVend.fermer();
        if(categories.size() == 0){
            Toast.makeText(AddProduitActivity.this,
                    "Vous n'avez aucune catégorie."
                    , Toast.LENGTH_LONG).show();
            Intent i = new Intent(AddProduitActivity.this,
                    AddCategorieActivity.class);
            startActivity(i);
        }
        else if(vendeurs.size() == 0){
            Toast.makeText(AddProduitActivity.this,
                    "Vous n'avez aucun vendeur."
                    , Toast.LENGTH_LONG).show();
            Intent i = new Intent(AddProduitActivity.this,
                    AddVendeurActivity.class);
            startActivity(i);
        }
        ItemCategorieAdaptateur itemCat = new ItemCategorieAdaptateur();
        ItemVendeurAdaptateur itemVend = new ItemVendeurAdaptateur();
        spCategorie.setAdapter(itemCat);
        spVendeur.setAdapter(itemVend);
        View.OnClickListener ajouter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("") ||
                etQuantite.getText().toString().equals("") ||
                etAlert.getText().toString().equals("") ||
                etPrix.getText().toString().equals("")){
                    tvError.setText("Tout les champs doivent être remplis.");
                }
                else{
                    Produit produit = new Produit();
                    produit.setId(idProduit);
                    produit.setNom(etNom.getText().toString());
                    produit.setQuantite(Integer.parseInt(etQuantite.getText().toString()));
                    produit.setAlert(Integer.parseInt(etAlert.getText().toString()));
                    produit.setPrix(Double.parseDouble(etPrix.getText().toString()));
                    produit.setCategoerie((Categorie) spCategorie.getSelectedItem());
                    produit.setVendeur((Vendeur) spVendeur.getSelectedItem());
                    ProduitDAO dbProd = new ProduitDAO(AddProduitActivity.this);
                    dbProd.ouvrir();
                    dbProd.addProduit(produit);
                    dbProd.fermer();
                    Toast.makeText(AddProduitActivity.this,
                            "Produit ajouté avec succès."
                            , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(AddProduitActivity.this,
                            MainActivity.class);
                    startActivity(i);
                }
            }
        };
        btnAjouter.setOnClickListener(ajouter);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(AddProduitActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

    class ItemVendeurAdaptateur extends ArrayAdapter<Vendeur> {

        ItemVendeurAdaptateur(){
            super(
                    AddProduitActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    vendeurs
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }

    class ItemCategorieAdaptateur extends ArrayAdapter<Categorie> {
        ItemCategorieAdaptateur(){
            super(
                    AddProduitActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    categories
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }

}
