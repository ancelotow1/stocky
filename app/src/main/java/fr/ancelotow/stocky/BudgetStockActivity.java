package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;

import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.technical.Session;

public class BudgetStockActivity extends AppCompatActivity {

    Button btnTerminer;
    TextView tvError;
    EditText etBudget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_stock);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(Session.getSession().getStock().getNom());
        btnTerminer = (Button) findViewById(R.id.btnTerminer);
        tvError = (TextView) findViewById(R.id.tvError);
        etBudget = (EditText) findViewById(R.id.etBudget);
        etBudget.setText(Double.toString(Session.getSession().getStock().getBudget()));
        View.OnClickListener budgeter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etBudget.getText().toString().equals("")){
                    tvError.setText("Le champ budget est obligatoire.");
                }
                else{
                    Session.getSession().getStock().setBudget(
                            Double.parseDouble(etBudget.getText().toString()));
                    StockDAO db = new StockDAO(BudgetStockActivity.this);
                    db.ouvrir();
                    db.setStockBudget(Session.getSession().getStock());
                    db.fermer();
                    Toast.makeText(BudgetStockActivity.this,
                            "Le budget du stock à été modifier avec succès."
                            , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(BudgetStockActivity.this,
                            MainActivity.class);
                    startActivity(i);
                }
            }
        };
        btnTerminer.setOnClickListener(budgeter);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(BudgetStockActivity.this,
                ParametreActivity.class);
        startActivity(i);
        return true;
    }

}
