package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.FactureDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Facture;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class PassageCourseActivity extends AppCompatActivity {

    boolean action = Session.getSession().getAction();
    ItemProduitAdaptateur item;
    ListView lvProduits;
    TextView tvTotal;
    Button btnTerminer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passage_course);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvProduits = (ListView) findViewById(R.id.lvProduits);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        btnTerminer = (Button) findViewById(R.id.btnTerminer);
        item = new ItemProduitAdaptateur();
        lvProduits.setAdapter(item);
        tvTotal.setText("TOTAL : " + Double.toString(Session.getSession().prixTotal()) + "€");
        lvProduits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putString("idProduit", Session.getSession().getProduits().get(i).getId());
                Intent i2 = new Intent(PassageCourseActivity.this,
                        ProduitActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
        View.OnClickListener terminer = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                ProduitDAO db = new ProduitDAO(PassageCourseActivity.this);
                db.ouvrir();
                for(Produit unP : Session.getSession().getProduits()){
                    db.updateQuantite(unP);
                }
                db.fermer();
                Session.getSession().getStock().modifBudget(Session.getSession().prixTotal());
                Facture facture = new Facture();
                facture.setDate(LocalDate.now());
                facture.setMontant(Session.getSession().prixTotal());
                facture.setStock(Session.getSession().getStock());
                FactureDAO dbFac = new FactureDAO(PassageCourseActivity.this);
                dbFac.ouvrir();
                dbFac.addFacture(facture);
                dbFac.fermer();
                Session.getSession().terminerProduit();
                Intent i = new Intent(PassageCourseActivity.this,
                        CourseActivity.class);
                startActivity(i);
            }
        };
        btnTerminer.setOnClickListener(terminer);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(PassageCourseActivity.this,
                CourseActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_passage, menu);
        return true;
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.rbAjout:
                if (checked)
                action = false;
                Session.getSession().setAction(false);
                break;
            case R.id.rbEnlever:
                if (checked)
                action = true;
                Session.getSession().setAction(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menuAjouter ) {
            new com.google.zxing.integration.android.
                    IntentIntegrator(PassageCourseActivity.this)
                    .setPrompt("Scannerz le produit (Alignez la barre rouge avec le code)")
                    .initiateScan();
        }
        else if(item.getItemId() == R.id.menuTri){
            LayoutInflater factory = LayoutInflater.from(PassageCourseActivity.this);
            final View body = factory.inflate(R.layout.dialog_action, null);
            RadioButton rbAjout = (RadioButton) body.findViewById(R.id.rbAjout);
            RadioButton rbEnlever = (RadioButton) body.findViewById(R.id.rbEnlever);
            if(!action){
                rbAjout.setChecked(true);
            }
            else{
                rbEnlever.setChecked(true);
            }
            AlertDialog.Builder dialog = new AlertDialog.Builder(PassageCourseActivity.this);
            dialog.setTitle("Réglage du Scanner");
            dialog.setView(body);
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, intent);
        if(scanningResult != null){
            if(!action){
                String id = scanningResult.getContents();
                ProduitDAO db = new ProduitDAO(this);
                db.ouvrir();
                Produit produit = db.getProduit(id);
                if(produit == null){
                    Bundle pack = new Bundle();
                    pack.putString("idProduit", id);
                    Intent i2 = new Intent(PassageCourseActivity.this,
                            AddProduitActivity.class);
                    i2.putExtras(pack);
                    startActivity(i2);
                    db.fermer();
                }
                else{
                    produit.setQuantite(1);
                    Session.getSession().addProduit(produit);
                    Toast.makeText(PassageCourseActivity.this,
                            "Ajout " + produit.getNom() + " dans la liste de course réussi."
                            , Toast.LENGTH_LONG).show();
                    db.fermer();
                    tvTotal.setText("TOTAL : " +
                            Double.toString(Session.getSession().prixTotal()) + "€");
                    lvProduits.setAdapter(item);
                }
            }
            else{
                String id = scanningResult.getContents();
                ProduitDAO db = new ProduitDAO(this);
                db.ouvrir();
                Produit produit = db.getProduit(id);
                if(produit == null){
                    Toast.makeText(PassageCourseActivity.this,
                            "ERREUR : Ce produit n'existe pas."
                            , Toast.LENGTH_LONG).show();
                    db.fermer();
                }
                else{
                    boolean reussi = Session.getSession().delProduit(produit);
                    if(reussi){
                        Toast.makeText(PassageCourseActivity.this,
                                "Produit retiré avec succès."
                                , Toast.LENGTH_LONG).show();
                        db.fermer();
                        lvProduits.setAdapter(item);
                        tvTotal.setText("TOTAL : " +
                                Double.toString(Session.getSession().prixTotal()) + "€");
                    }
                    else{
                        Toast.makeText(PassageCourseActivity.this,
                                "Ce produit n'est pas dans la liste de course."
                                , Toast.LENGTH_LONG).show();
                        db.fermer();
                    }
                }
            }
        }
        else{
            Toast.makeText(PassageCourseActivity.this,
                    "Aucune donnée trouvée."
                    , Toast.LENGTH_LONG).show();
        }
    }

    class ItemProduitAdaptateur extends ArrayAdapter<Produit> {
        ItemProduitAdaptateur(){
            super(
                    PassageCourseActivity.this,
                    R.layout.item_course,
                    R.id.tvNom,
                    Session.getSession().getProduits()
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            TextView tvQuantite = (TextView) vItem.findViewById(R.id.tvQuantite);
            TextView tvPrix = (TextView) vItem.findViewById(R.id.tvPrix);
            tvQuantite.setText(Integer.toString(Session.getSession().getProduits().
                    get(position).getQuantite()));
            tvPrix.setText(Double.toString(Session.getSession().getProduits().
                    get(position).getPrix()));
            return vItem;
        }
    }

}
