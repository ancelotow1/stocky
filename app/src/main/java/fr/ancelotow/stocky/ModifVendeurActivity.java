package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class ModifVendeurActivity extends AppCompatActivity {

    TextView tvError;
    EditText etNom;
    EditText etTel;
    EditText etEmail;
    EditText etUrl;
    Button btnModif;
    Vendeur vendeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_vendeur);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final long idVendeur = pack.getLong("idVendeur");
        VendeurDAO db = new VendeurDAO(this);
        db.ouvrir();
        vendeur = db.getVendeur(idVendeur);
        db.fermer();
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        etTel = (EditText) findViewById(R.id.etTel);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnModif = (Button) findViewById(R.id.btnModifier);
        etNom.setText(vendeur.getNom());
        etEmail.setText(vendeur.getEmail());
        etTel.setText(vendeur.getTel());
        etUrl.setText(vendeur.getUrl());
        View.OnClickListener modifier = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("")){
                    tvError.setText("Veuillez renseignez le champ NOM.");
                }
                else{
                    vendeur.setNom(etNom.getText().toString());
                    vendeur.setTel(etTel.getText().toString());
                    vendeur.setEmail(etEmail.getText().toString());
                    vendeur.setUrl(etUrl.getText().toString());
                    vendeur.setStock(Session.getSession().getStock());
                    VendeurDAO db = new VendeurDAO(ModifVendeurActivity.this);
                    db.ouvrir();
                    db.modifVendeur(vendeur);
                    db.fermer();
                    Toast.makeText(ModifVendeurActivity.this,
                            "Vendeur modifier avec succès"
                            , Toast.LENGTH_LONG).show();
                    Bundle pack = new Bundle();
                    pack.putLong("idVendeur", idVendeur);
                    Intent i2 = new Intent(ModifVendeurActivity.this,
                            VendeurActivity.class);
                    i2.putExtras(pack);
                    startActivity(i2);
                }
            }
        };
        btnModif.setOnClickListener(modifier);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ModifVendeurActivity.this,
                ListVendeurActivity.class);
        startActivity(i);
        return true;
    }

}
