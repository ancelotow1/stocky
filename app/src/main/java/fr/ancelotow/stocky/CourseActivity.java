package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class CourseActivity extends AppCompatActivity {

    boolean tri;
    ListView lvCourse;
    List<Categorie> categories = new ArrayList<Categorie>();
    List<Vendeur> vendeurs = new ArrayList<Vendeur>();
    ItemVendeurAdaptateur itemVend;
    ItemCategorieAdaptateur itemCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tri = Session.getSession().getTri();
        lvCourse = (ListView) findViewById(R.id.lvCourse);
        CategorieDAO dbCat = new CategorieDAO(this);
        dbCat.ouvrir();
        categories = dbCat.getCategorieStock(Session.getSession().getStock().getId());
        dbCat.fermer();
        VendeurDAO dbVend = new VendeurDAO(this);
        dbVend.ouvrir();
        vendeurs = dbVend.getVendeurStock(Session.getSession().getStock().getId());
        dbVend.fermer();
        itemVend = new ItemVendeurAdaptateur();
        itemCat = new ItemCategorieAdaptateur();
        if(!tri){
            lvCourse.setAdapter(itemVend);
        }
        else{
            lvCourse.setAdapter(itemCat);
        }
        lvCourse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                if(!tri){
                    pack.putLong("id", vendeurs.get(i).getId());
                }
                else{
                    pack.putLong("id", categories.get(i).getId());
                }
                Intent i2 = new Intent(CourseActivity.this,
                        ListCourseActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.rbVendeur:
                if (checked)
                    lvCourse.setAdapter(itemVend);
                    tri = false;
                    Session.getSession().setTri(false);
                    break;
            case R.id.rbCategorie:
                if (checked)
                    lvCourse.setAdapter(itemCat);
                    tri = true;
                    Session.getSession().setTri(true);
                    break;
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(CourseActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menuCourse ) {
            Intent i = new Intent(CourseActivity.this,
                    PassageCourseActivity.class);
            startActivity(i);
        }
        else if(item.getItemId() == R.id.menuTri){
            LayoutInflater factory = LayoutInflater.from(CourseActivity.this);
            final View body = factory.inflate(R.layout.dialog_tri, null);
            RadioButton rbVendeur = (RadioButton) body.findViewById(R.id.rbVendeur);
            RadioButton rbCategorie = (RadioButton) body.findViewById(R.id.rbCategorie);
            if(!tri){
                rbVendeur.setChecked(true);
            }
            else{
                rbCategorie.setChecked(true);
            }
            AlertDialog.Builder dialog = new AlertDialog.Builder(CourseActivity.this);
            dialog.setTitle("Réglage du tri");
            dialog.setView(body);
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }


    class ItemVendeurAdaptateur extends ArrayAdapter<Vendeur> {

        ItemVendeurAdaptateur(){
            super(
                    CourseActivity.this,
                    R.layout.item_vendeurcourse,
                    R.id.tvNom,
                    vendeurs
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }


    class ItemCategorieAdaptateur extends ArrayAdapter<Categorie> {
        ItemCategorieAdaptateur(){
            super(
                    CourseActivity.this,
                    R.layout.item_categories,
                    R.id.tvNom,
                    categories
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            ImageView ivImg = (ImageView) vItem.findViewById(R.id.ivImage);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            if(categories.get(position).getImage().equals("aucune")){
                LinearLayout.LayoutParams paramIv =
                        new LinearLayout.LayoutParams(0, 0);
                LinearLayout.LayoutParams paramTv =
                        new LinearLayout.LayoutParams(150, 200);
                ivImg.setLayoutParams(paramIv);
                tvNom.setLayoutParams(paramTv);
            }
            else{
                Bitmap bitmap = BitmapFactory.decodeFile(categories.get(position).getImage());
                ivImg.setImageBitmap(bitmap);
            }
            return vItem;
        }
    }

}
