package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class ModifProduitActivity extends AppCompatActivity {

    TextView tvError;
    EditText etNom;
    EditText etPrix;
    EditText etQuantite;
    EditText etAlert;
    Spinner spCategorie;
    Spinner spVendeur;
    Button btnModif;
    List<Categorie> categories = new ArrayList<Categorie>();
    List<Vendeur> vendeurs = new ArrayList<Vendeur>();
    Produit produit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_produit);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final String idProd = pack.getString("idProduit");
        final ProduitDAO dbProd = new ProduitDAO(this);
        dbProd.ouvrir();
        produit = dbProd.getProduit(idProd);
        dbProd.fermer();
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        etQuantite = (EditText) findViewById(R.id.etQuantite);
        etAlert = (EditText) findViewById(R.id.etAlert);
        etPrix = (EditText) findViewById(R.id.etPrix);
        spCategorie = (Spinner) findViewById(R.id.spCategorie);
        spVendeur = (Spinner) findViewById(R.id.spVendeur);
        btnModif = (Button) findViewById(R.id.btnModifier);
        etNom.setText(produit.getNom());
        etQuantite.setText(Integer.toString(produit.getQuantite()));
        etAlert.setText(Integer.toString(produit.getAlert()));
        etPrix.setText(Double.toString(produit.getPrix()));
        CategorieDAO dbCat = new CategorieDAO(this);
        dbCat.ouvrir();
        categories = dbCat.getCategorieStock(Session.getSession().getStock().getId());
        dbCat.fermer();
        VendeurDAO dbVend = new VendeurDAO(this);
        dbVend.ouvrir();
        vendeurs = dbVend.getVendeurStock(Session.getSession().getStock().getId());
        dbVend.fermer();
        ItemCategorieAdaptateur itemCat = new ItemCategorieAdaptateur();
        ItemVendeurAdaptateur itemVend = new ItemVendeurAdaptateur();
        spCategorie.setAdapter(itemCat);
        spVendeur.setAdapter(itemVend);
        View.OnClickListener modifier = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("") ||
                        etQuantite.getText().toString().equals("") ||
                        etAlert.getText().toString().equals("") ||
                        etPrix.getText().toString().equals("")){
                    tvError.setText("Tout les champs doivent être remplis.");
                }
                else{
                    produit.setNom(etNom.getText().toString());
                    produit.setQuantite(Integer.parseInt(etQuantite.getText().toString()));
                    produit.setAlert(Integer.parseInt(etAlert.getText().toString()));
                    produit.setPrix(Double.parseDouble(etPrix.getText().toString()));
                    produit.setCategoerie((Categorie) spCategorie.getSelectedItem());
                    produit.setVendeur((Vendeur) spVendeur.getSelectedItem());
                    dbProd.ouvrir();
                    dbProd.modifProduit(produit);
                    dbProd.fermer();
                    Toast.makeText(ModifProduitActivity.this,
                            "Produit ajouté avec succès."
                            , Toast.LENGTH_LONG).show();
                    Bundle pack = new Bundle();
                    pack.putString("idProduit", produit.getId());
                    Intent i2 = new Intent(ModifProduitActivity.this,
                            ProduitActivity.class);
                    i2.putExtras(pack);
                    startActivity(i2);
                }
            }
        };
        btnModif.setOnClickListener(modifier);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Bundle pack = new Bundle();
        pack.putString("idProduit", produit.getId());
        Intent i2 = new Intent(ModifProduitActivity.this,
                ProduitActivity.class);
        i2.putExtras(pack);
        startActivity(i2);
        return true;
    }

    class ItemVendeurAdaptateur extends ArrayAdapter<Vendeur> {
        ItemVendeurAdaptateur(){
            super(
                    ModifProduitActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    vendeurs
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }

    class ItemCategorieAdaptateur extends ArrayAdapter<Categorie> {
        ItemCategorieAdaptateur(){
            super(
                    ModifProduitActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    categories
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }

}
