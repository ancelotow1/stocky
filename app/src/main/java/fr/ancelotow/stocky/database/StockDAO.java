package fr.ancelotow.stocky.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.entity.Stock;

public class StockDAO {

    private static final String TABLE_NAME = "stockyDB";
    public static final String COL_ID_PK = "stock_id";
    public static final String COL_NOM = "stock_nom";
    public static final String COL_BUDGET = "stock_budget";
    public static final String COL_UTIL = "stock_util";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            COL_NOM+" TEXT, \n" +
            COL_BUDGET+" DOUBLE, \n" +
            COL_UTIL+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public StockDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    public long addStock(Stock stock) {
        ContentValues values = new ContentValues();
        values.put(COL_NOM, stock.getNom());
        values.put(COL_BUDGET, stock.getBudget());
        values.put(COL_UTIL, 1);
        return db.insert(TABLE_NAME,null,values);
    }

    public int supStock(Stock stock) {
        String where = COL_ID_PK+" = ?";
        String[] whereArgs = {stock.getId()+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int setStockName(Stock stock) {
        ContentValues values = new ContentValues();
        values.put(COL_NOM, stock.getNom());
        return db.update(TABLE_NAME,values,
                COL_ID_PK + " = " +stock.getId(),
                null);
    }

    public int setStockBudget(Stock stock) {
        ContentValues values = new ContentValues();
        values.put(COL_BUDGET, stock.getBudget());
        return db.update(TABLE_NAME,values,
                COL_ID_PK + " = " +stock.getId(),
                null);
    }

    public int setStockUtil(Stock stock) {
        ContentValues values = new ContentValues();
        values.put(COL_UTIL, 0);
        return db.update(TABLE_NAME,values,
                COL_ID_PK + " = " +stock.getId(),
                null);
    }

    public int setStockUtilUse(Stock stock) {
        ContentValues values = new ContentValues();
        values.put(COL_UTIL, 1);
        return db.update(TABLE_NAME,values,
                COL_ID_PK + " = " +stock.getId(),
                null);
    }

    public Stock getStock(int id){
        Stock stock = new Stock();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+id+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        if (cur.moveToFirst()) {
            stock.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            stock.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            stock.setBudget(cur.getDouble(cur.getColumnIndex(COL_BUDGET)));
            int util = cur.getInt(cur.getColumnIndex(COL_UTIL));
            boolean use;
            if(util == 0){
                use = false;
            } else{
                use = true;
            }
            stock.setUtil(use);
            cur.close();
        }
        return stock;
    }

    public List<Stock> getStocks(){
        List<Stock> stocks = new ArrayList<Stock>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex(COL_ID_PK));
            String nom = cur.getString(cur.getColumnIndex(COL_NOM));
            Double budget = cur.getDouble(cur.getColumnIndex(COL_BUDGET));
            Stock unS = new Stock(id, nom, budget);
            int util = cur.getInt(cur.getColumnIndex(COL_UTIL));
            boolean use;
            if(util == 0){
                use = false;
            } else{
                use = true;
            }
            unS.setUtil(use);
            stocks.add(unS);
        }
        return stocks;
    }


}
