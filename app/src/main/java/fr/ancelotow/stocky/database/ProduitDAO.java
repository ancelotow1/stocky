package fr.ancelotow.stocky.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;

public class ProduitDAO {

    private static final String TABLE_NAME = "produitDB";
    public static final String COL_ID_PK = "pro_id";
    public static final String COL_NOM = "pro_nom";
    public static final String COL_IMG = "pro_img";
    public static final String COL_QUANTITE = "pro_quantite";
    public static final String COL_ALERT = "pro_alert";
    public static final String COL_PRIX = "pro_prix";
    public static final String COL_CAT_FK = "cat_id";
    public static final String COL_VEND_FK = "vend_id";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" TEXT PRIMARY KEY, \n" +
            COL_NOM+" TEXT, \n" +
            COL_IMG+" TEXT, \n" +
            COL_QUANTITE+" INTEGER, \n" +
            COL_ALERT+" INTEGER, \n" +
            COL_PRIX+" DOUBLE, \n" +
            COL_CAT_FK+" INTEGER, \n" +
            COL_VEND_FK+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public ProduitDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    public long addProduit(Produit produit) {
        ContentValues values = new ContentValues();
        values.put(COL_ID_PK, produit.getId());
        values.put(COL_NOM, produit.getNom());
        values.put(COL_IMG, produit.getImage());
        values.put(COL_QUANTITE, produit.getQuantite());
        values.put(COL_ALERT, produit.getAlert());
        values.put(COL_PRIX, produit.getPrix());
        values.put(COL_CAT_FK, produit.getCategoerie().getId());
        values.put(COL_VEND_FK, produit.getVendeur().getId());
        return db.insert(TABLE_NAME,null,values);
    }

    public int supProduit(Produit produit) {
        String where = COL_ID_PK+" = ?";
        String[] whereArgs = {produit.getId()+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int supProduitCat(long id) {
        String where = COL_CAT_FK+" = ?";
        String[] whereArgs = {String.valueOf(id)};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int supProduitVend(long id) {
        String where = COL_VEND_FK+" = ?";
        String[] whereArgs = {String.valueOf(id)};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int modifProduit(Produit produit) {
        String where = COL_ID_PK+" = ?";
        ContentValues values = new ContentValues();
        values.put(COL_NOM, produit.getNom());
        values.put(COL_IMG, produit.getImage());
        values.put(COL_QUANTITE, produit.getQuantite());
        values.put(COL_ALERT, produit.getAlert());
        values.put(COL_PRIX, produit.getPrix());
        values.put(COL_CAT_FK, produit.getCategoerie().getId());
        values.put(COL_VEND_FK, produit.getVendeur().getId());
        String[] whereArgs = {produit.getId()+""};
        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public Produit getProduit(String id){
        Vendeur vendeur = new Vendeur();
        Categorie cat = new Categorie();
        Produit produit = null;
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+id+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        if (cur.moveToFirst()) {
            produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getInt(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            cur.close();
        }
        return produit;
    }

    public List<Produit> getProduits(){
        List<Produit> produits = new ArrayList<Produit>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        while(cur.moveToNext()){
            Vendeur vendeur = new Vendeur();
            Categorie cat = new Categorie();
            Produit produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getLong(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            produits.add(produit);
        }
        return produits;
    }

    public int updateQuantite(Produit produit){
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+produit.getId()+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        if (cur.moveToFirst()) {
            String where = COL_ID_PK + " = ?";
            ContentValues values = new ContentValues();
            values.put(COL_QUANTITE, produit.getQuantite() +
                    cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            String[] whereArgs = {produit.getId()+""};
            return db.update(TABLE_NAME, values, where, whereArgs);
        }
        return 0;
    }

    public List<Produit> getProduitsCat(long idCat){
        List<Produit> produits = new ArrayList<Produit>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_CAT_FK+"="+idCat+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Vendeur vendeur = new Vendeur();
            Categorie cat = new Categorie();
            Produit produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getInt(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            produits.add(produit);
        }
        return produits;
    }

    public List<Produit> getRupProduitsCat(long idCat){
        List<Produit> produits = new ArrayList<Produit>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_CAT_FK+"="+idCat+ " " +
                        "AND "+COL_QUANTITE+" <= "+COL_ALERT+" "+
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Vendeur vendeur = new Vendeur();
            Categorie cat = new Categorie();
            Produit produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getInt(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            produits.add(produit);
        }
        return produits;
    }

    public List<Produit> getRupProduitsVend(long idVend){
        List<Produit> produits = new ArrayList<Produit>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_VEND_FK+"="+idVend+ " " +
                        "AND "+COL_QUANTITE+" <= "+COL_ALERT+" "+
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Vendeur vendeur = new Vendeur();
            Categorie cat = new Categorie();
            Produit produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getInt(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            produits.add(produit);
        }
        return produits;
    }

    public List<Produit> getProduitsStock(List<Integer> idCats){
        List<Produit> produits = new ArrayList<Produit>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_CAT_FK+" like "+idCats+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Vendeur vendeur = new Vendeur();
            Categorie cat = new Categorie();
            Produit produit = new Produit();
            produit.setId(cur.getString(cur.getColumnIndex(COL_ID_PK)));
            produit.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            produit.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            produit.setQuantite(cur.getInt(cur.getColumnIndex(COL_QUANTITE)));
            produit.setAlert(cur.getInt(cur.getColumnIndex(COL_ALERT)));
            produit.setPrix(cur.getDouble(cur.getColumnIndex(COL_PRIX)));
            cat.setId(cur.getInt(cur.getColumnIndex(COL_CAT_FK)));
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_VEND_FK)));
            produit.setCategoerie(cat);
            produit.setVendeur(vendeur);
            produits.add(produit);
        }
        return produits;
    }

}
