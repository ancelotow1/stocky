package fr.ancelotow.stocky.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.entity.Stock;
import fr.ancelotow.stocky.entity.Categorie;

public class CategorieDAO {

    private static final String TABLE_NAME = "categorieDB";
    public static final String COL_ID_PK = "cat_id";
    public static final String COL_NOM = "cat_nom";
    public static final String COL_IMG = "cat_img";
    public static final String COL_STOCK_FK = "stock_id";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            COL_NOM+" TEXT, \n" +
            COL_IMG+" TEXT, \n" +
            COL_STOCK_FK+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public CategorieDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    public long addCategorie(Categorie cat) {
        ContentValues values = new ContentValues();
        values.put(COL_NOM, cat.getNom());
        values.put(COL_IMG, cat.getImage());
        values.put(COL_STOCK_FK, cat.getStock().getId());
        return db.insert(TABLE_NAME,null,values);
    }

    public int supCategorie(Categorie cat) {
        String where = COL_ID_PK+" = ?";
        String[] whereArgs = {cat.getId()+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int supCategorieStock(long id) {
        String where = COL_STOCK_FK+" = ?";
        String[] whereArgs = {id+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int modifCategorie(Categorie cat) {
        String where = COL_ID_PK+" = ?";
        ContentValues values = new ContentValues();
        values.put(COL_NOM, cat.getNom());
        values.put(COL_IMG, cat.getImage());
        String[] whereArgs = {cat.getId()+""};
        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public Categorie getCategorie(long id){
        Stock stock = new Stock();
        Categorie cat = new Categorie();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+id+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        if (cur.moveToFirst()) {
            cat.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            cat.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            cat.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            cat.setStock(stock);
            cur.close();
        }
        return cat;
    }

    public List<Categorie> getCategories(){
        List<Categorie> categories = new ArrayList<Categorie>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        while(cur.moveToNext()){
            Categorie unC = new Categorie();
            Stock stock = new Stock();
            unC.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            unC.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            unC.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            unC.setStock(stock);
            categories.add(unC);
        }
        return categories;
    }

    public List<Categorie> getCategorieStock(long idStock){
        List<Categorie> categories = new ArrayList<Categorie>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_STOCK_FK+"="+idStock+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Categorie unC = new Categorie();
            Stock stock = new Stock();
            unC.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            unC.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            unC.setImage(cur.getString(cur.getColumnIndex(COL_IMG)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            unC.setStock(stock);
            categories.add(unC);
        }
        return categories;
    }

    public List<Long> getIdCategorieStock(long idStock){
        List<Long> idCategories = new ArrayList<Long>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_STOCK_FK+" = "+idStock+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            idCategories.add(cur.getLong(cur.getColumnIndex(COL_ID_PK)));
        }
        return idCategories;
    }

}
