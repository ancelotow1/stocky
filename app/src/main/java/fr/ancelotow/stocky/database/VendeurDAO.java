package fr.ancelotow.stocky.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.entity.Stock;
import fr.ancelotow.stocky.entity.Vendeur;

public class VendeurDAO {

    private static final String TABLE_NAME = "vendeurDB";
    public static final String COL_ID_PK = "vend_id";
    public static final String COL_NOM = "vend_nom";
    public static final String COL_TEL = "vend_tel";
    public static final String COL_MAIL = "vend_email";
    public static final String COL_URL = "vend_url";
    public static final String COL_STOCK_FK = "stock_id";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            COL_NOM+" TEXT, \n" +
            COL_TEL+" TEXT, \n" +
            COL_MAIL+" TEXT, \n" +
            COL_URL+" TEXT, \n" +
            COL_STOCK_FK+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public VendeurDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    public long addVendeur(Vendeur vendeur) {
        ContentValues values = new ContentValues();
        values.put(COL_NOM, vendeur.getNom());
        values.put(COL_TEL, vendeur.getTel());
        values.put(COL_MAIL, vendeur.getEmail());
        values.put(COL_URL, vendeur.getUrl());
        values.put(COL_STOCK_FK, vendeur.getStock().getId());
        return db.insert(TABLE_NAME,null,values);
    }

    public int supVendeur(Vendeur vendeur) {
        String where = COL_ID_PK+" = ?";
        String[] whereArgs = {vendeur.getId()+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int supVendeurStock(long id) {
        String where = COL_STOCK_FK+" = ?";
        String[] whereArgs = {id+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public int modifVendeur(Vendeur vendeur) {
        String where = COL_ID_PK+" = ?";
        ContentValues values = new ContentValues();
        values.put(COL_NOM, vendeur.getNom());
        values.put(COL_TEL, vendeur.getTel());
        values.put(COL_MAIL, vendeur.getEmail());
        values.put(COL_URL, vendeur.getUrl());
        String[] whereArgs = {vendeur.getId()+""};
        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public Vendeur getVendeur(long id){
        Stock stock = new Stock();
        Vendeur vendeur = new Vendeur();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+id+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        if (cur.moveToFirst()) {
            vendeur.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            vendeur.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            vendeur.setTel(cur.getString(cur.getColumnIndex(COL_TEL)));
            vendeur.setEmail(cur.getString(cur.getColumnIndex(COL_MAIL)));
            vendeur.setUrl(cur.getString(cur.getColumnIndex(COL_URL)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            vendeur.setStock(stock);
            cur.close();
        }
        return vendeur;
    }

    public List<Vendeur> getVendeur(){
        List<Vendeur> vendeurs = new ArrayList<Vendeur>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        while(cur.moveToNext()){
            Vendeur unV = new Vendeur();
            Stock stock = new Stock();
            unV.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            unV.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            unV.setTel(cur.getString(cur.getColumnIndex(COL_TEL)));
            unV.setEmail(cur.getString(cur.getColumnIndex(COL_MAIL)));
            unV.setUrl(cur.getString(cur.getColumnIndex(COL_URL)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            unV.setStock(stock);
            vendeurs.add(unV);
        }
        return vendeurs;
    }

    public List<Vendeur> getVendeurStock(long idStock){
        List<Vendeur> vendeurs = new ArrayList<Vendeur>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_STOCK_FK+"="+idStock+ " " +
                        "ORDER BY "+COL_NOM, null
        );
        while(cur.moveToNext()){
            Vendeur unV = new Vendeur();
            Stock stock = new Stock();
            unV.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            unV.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            unV.setTel(cur.getString(cur.getColumnIndex(COL_TEL)));
            unV.setEmail(cur.getString(cur.getColumnIndex(COL_MAIL)));
            unV.setUrl(cur.getString(cur.getColumnIndex(COL_URL)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            unV.setStock(stock);
            vendeurs.add(unV);
        }
        return vendeurs;
    }

}
