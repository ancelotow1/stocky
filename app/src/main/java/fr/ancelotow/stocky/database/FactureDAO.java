package fr.ancelotow.stocky.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ancelotow.stocky.entity.Facture;
import fr.ancelotow.stocky.entity.Stock;

public class FactureDAO {

    private static final String TABLE_NAME = "factureDB";
    public static final String COL_ID_PK = "fac_id";
    public static final String COL_DATE = "fac_date";
    public static final String COL_MONTANT = "fac_montant";
    public static final String COL_STOCK_FK = "stock_id";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            COL_DATE+" TEXT, \n" +
            COL_MONTANT+" DOUBLE, \n" +
            COL_STOCK_FK+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public FactureDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    public int supFactureStock(long id) {
        String where = COL_STOCK_FK+" = ?";
        String[] whereArgs = {id+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public long addFacture(Facture facture) {
        ContentValues values = new ContentValues();
        values.put(COL_DATE, facture.getDate().toString());
        values.put(COL_MONTANT, facture.getMontant());
        values.put(COL_STOCK_FK, facture.getStock().getId());
        return db.insert(TABLE_NAME,null,values);
    }

    public int modifFacture(Facture facture) {
        String where = COL_ID_PK+" = ?";
        ContentValues values = new ContentValues();
        values.put(COL_MONTANT, facture.getMontant());
        String[] whereArgs = {facture.getId()+""};
        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public Facture getFacture(long id) throws ParseException {
        Stock stock = new Stock();
        Facture facture = new Facture();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_ID_PK+"="+id+ " " +
                        "ORDER BY "+COL_DATE+" DESC", null
        );
        if (cur.moveToFirst()) {
            facture.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            String strDate = cur.getString(cur.getColumnIndex(COL_DATE));
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
            facture.setDate(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            facture.setMontant(cur.getDouble(cur.getColumnIndex(COL_MONTANT)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            facture.setStock(stock);
            cur.close();
        }
        return facture;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Facture> getFacture() throws ParseException {
        List<Facture> lesFactures = new ArrayList<Facture>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME+" ORDER BY "+COL_DATE+" DESC",
                null);
        while(cur.moveToNext()){
            Facture facture = new Facture();
            Stock stock = new Stock();
            facture.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            String strDate = cur.getString(cur.getColumnIndex(COL_DATE));
            facture.setDate(LocalDate.parse(strDate));
            facture.setMontant(cur.getDouble(cur.getColumnIndex(COL_MONTANT)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            facture.setStock(stock);;
            lesFactures.add(facture);
        }
        return lesFactures;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Facture> getFactureStock(long idStock) throws ParseException{
        List<Facture> lesFactures = new ArrayList<Facture>();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                        "FROM "+TABLE_NAME+" " +
                        "WHERE "+COL_STOCK_FK+"="+idStock+ " " +
                        "ORDER BY "+COL_DATE+" DESC", null
        );
        while(cur.moveToNext()){
            Facture facture = new Facture();
            Stock stock = new Stock();
            facture.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            String strDate = cur.getString(cur.getColumnIndex(COL_DATE));
            facture.setDate(LocalDate.parse(strDate));
            facture.setMontant(cur.getDouble(cur.getColumnIndex(COL_MONTANT)));
            stock.setId(cur.getInt(cur.getColumnIndex(COL_STOCK_FK)));
            facture.setStock(stock);
            lesFactures.add(facture);
        }
        return lesFactures;
    }

}
