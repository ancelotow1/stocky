package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class ListVendeurActivity extends AppCompatActivity {

    List<Vendeur> vendeurs = new ArrayList<Vendeur>();
    ListView lvVendeurs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_vendeur);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvVendeurs = (ListView) findViewById(R.id.lvVendeurs);
        VendeurDAO db = new VendeurDAO(this);
        db.ouvrir();
        vendeurs = db.getVendeurStock(Session.getSession().getStock().getId());
        db.fermer();
        ListVendeurActivity.ItemVendeurAdaptateur items =
                new ListVendeurActivity.ItemVendeurAdaptateur();
        lvVendeurs.setAdapter(items);
        lvVendeurs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putLong("idVendeur", vendeurs.get(i).getId());
                Intent i2 = new Intent(ListVendeurActivity.this,
                        VendeurActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ListVendeurActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuAdd:
                Intent i = new Intent(ListVendeurActivity.this,
                    AddVendeurActivity.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    class ItemVendeurAdaptateur extends ArrayAdapter<Vendeur> {

        ItemVendeurAdaptateur(){
            super(
                    ListVendeurActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    vendeurs
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }




}
