package fr.ancelotow.stocky;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import java.util.List;

import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.technical.Session;
import fr.ancelotow.stocky.entity.Stock;

public class LoadActivity extends AppCompatActivity {

    ProgressBar pbApp;
    private final static int TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
        pbApp = (ProgressBar) findViewById(R.id.pbApp);
        pbApp.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.colorLight),
                android.graphics.PorterDuff.Mode.SRC_IN
        );
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                StockDAO db = new StockDAO(LoadActivity.this);
                db.ouvrir();
                List<Stock> stocks = db.getStocks();
                db.fermer();
                if(stocks.size() == 0){
                    Intent i = new Intent(LoadActivity.this,
                            CreateStockActivity.class);
                    startActivity(i);
                }
                else {
                    Stock stock = null;
                    for(Stock unS : stocks){
                        if(unS.isUtil()){
                            stock = unS;
                        }
                    }
                    if(stock == null){
                        Intent i = new Intent(LoadActivity.this,
                                ChoiceStockActivity.class);
                        startActivity(i);
                    }
                    else{
                        Session.ouvrir(stock);
                        Intent i = new Intent(LoadActivity.this, MainActivity.class);
                        startActivity(i);
                    }
                }
                finish();
            }
        }, TIME_OUT);
    }

}
