package fr.ancelotow.stocky;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import static android.Manifest.permission.CALL_PHONE;

import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Vendeur;

public class VendeurActivity extends AppCompatActivity {

    TextView tvNom;
    TextView tvTel;
    TextView tvMail;
    TextView tvUrl;
    Button btnUrl, btnModif, btnSup, btnTel, btnMail;
    Vendeur vendeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendeur);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final long idVendeur = pack.getLong("idVendeur");
        tvNom = (TextView) findViewById(R.id.tvNom);
        tvTel = (TextView) findViewById(R.id.tvTel);
        tvMail = (TextView) findViewById(R.id.tvMail);
        tvUrl = (TextView) findViewById(R.id.tvUrl);
        btnUrl = (Button) findViewById(R.id.btnUrl);
        btnModif = (Button) findViewById(R.id.btnModifier);
        btnSup = (Button) findViewById(R.id.btnSupprimer);
        btnTel = (Button) findViewById(R.id.btnTel);
        btnMail = (Button) findViewById(R.id.btnMail);
        VendeurDAO db = new VendeurDAO(this);
        db.ouvrir();
        vendeur = db.getVendeur(idVendeur);
        db.fermer();
        tvNom.setText(vendeur.getNom());
        tvTel.setText(vendeur.getTel());
        tvMail.setText(vendeur.getEmail());
        tvUrl.setText(vendeur.getUrl());
        final AlertDialog.Builder alertSup = new AlertDialog.Builder(this);
        alertSup.setPositiveButton(R.string.btnOuiSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                ProduitDAO dbProd = new ProduitDAO(VendeurActivity.this);
                dbProd.ouvrir();
                dbProd.supProduitVend(vendeur.getId());
                dbProd.fermer();
                VendeurDAO db = new VendeurDAO(VendeurActivity.this);
                db.ouvrir();
                db.supVendeur(vendeur);
                db.fermer();
                Toast.makeText(VendeurActivity.this,
                        "Vendeur supprimé avec succès."
                        , Toast.LENGTH_LONG).show();
                Intent intentionEnvoyer = new Intent(VendeurActivity.this,
                        ListVendeurActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        alertSup.setNegativeButton(R.string.btnNonSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertSup.setMessage(R.string.tvSupVendeur);
        View.OnClickListener internet = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(vendeur.getUrl()));
                    startActivity(i);
                }
                catch(Exception e){
                    Toast.makeText(VendeurActivity.this,
                            "Cette adresse internet n'est pas valide."
                            , Toast.LENGTH_LONG).show();
                }
            }
        };
        View.OnClickListener modifier = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle pack = new Bundle();
                pack.putLong("idVendeur", idVendeur);
                Intent i2 = new Intent(VendeurActivity.this,
                        ModifVendeurActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        };
        View.OnClickListener email = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.setType("message/plain");
                email.putExtra(Intent.EXTRA_EMAIL,vendeur.getEmail());
                email.putExtra(Intent.EXTRA_SUBJECT, "Saisir le sujet ici...");
                email.putExtra(Intent.EXTRA_TEXT, "Saisir votre  message ici...");
                email.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(email, "Choisir l'application"));
            }
        };
        View.OnClickListener telephoner = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + vendeur.getTel()));
                if (ContextCompat.checkSelfPermission(VendeurActivity.this,
                        CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(callIntent);
                } else {
                    requestPermissions(new String[]{CALL_PHONE}, 1);
                }
            }
        };
        View.OnClickListener supprimer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSup.show();
            }
        };
        btnUrl.setOnClickListener(internet);
        btnModif.setOnClickListener(modifier);
        btnSup.setOnClickListener(supprimer);
        btnTel.setOnClickListener(telephoner);
        btnMail.setOnClickListener(email);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intentionEnvoyer = new Intent(VendeurActivity.this,
                ListVendeurActivity.class);
        startActivity(intentionEnvoyer);
        return true;
    }

}
