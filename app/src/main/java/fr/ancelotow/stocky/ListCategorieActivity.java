package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.technical.Session;

public class ListCategorieActivity extends AppCompatActivity {

    List<Categorie> categories = new ArrayList<Categorie>();
    ListView lvCats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_categorie);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvCats = (ListView) findViewById(R.id.lvCats);
        CategorieDAO db = new CategorieDAO(this);
        db.ouvrir();
        categories = db.getCategorieStock(Session.getSession().getStock().getId());
        db.fermer();
        ItemCategorieAdaptateur item = new ItemCategorieAdaptateur();
        lvCats.setAdapter(item);
        lvCats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putLong("idCategorie", categories.get(i).getId());
                Intent i2 = new Intent(ListCategorieActivity.this,
                        CategorieActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ListCategorieActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuAdd:
                Intent i = new Intent(ListCategorieActivity.this,
                        AddCategorieActivity.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    class ItemCategorieAdaptateur extends ArrayAdapter<Categorie> {
        ItemCategorieAdaptateur(){
            super(
                    ListCategorieActivity.this,
                    R.layout.item_categories,
                    R.id.tvNom,
                    categories
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            ImageView ivImg = (ImageView) vItem.findViewById(R.id.ivImage);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            if(categories.get(position).getImage().equals("aucune")){
                LinearLayout.LayoutParams paramIv =
                        new LinearLayout.LayoutParams(0, 0);
                LinearLayout.LayoutParams paramTv =
                        new LinearLayout.LayoutParams(150, 200);
                ivImg.setLayoutParams(paramIv);
                tvNom.setLayoutParams(paramTv);
            }
            else{
                Bitmap bitmap = BitmapFactory.decodeFile(categories.get(position).getImage());
                ivImg.setImageBitmap(bitmap);
            }
            return vItem;
        }
    }

}
