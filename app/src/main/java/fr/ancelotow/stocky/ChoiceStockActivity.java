package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.technical.Session;
import fr.ancelotow.stocky.entity.Stock;

public class ChoiceStockActivity extends AppCompatActivity {

    Button btnAdd;
    ListView lvStock;
    protected List<Stock> stocks = new ArrayList<Stock>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_stock);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        lvStock = (ListView) findViewById(R.id.lvStock);
        StockDAO db = new StockDAO(this);
        db.ouvrir();
        stocks = db.getStocks();
        db.fermer();
        ItemStockAdaptateur items = new ItemStockAdaptateur();
        lvStock.setAdapter(items);
        lvStock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Stock stock = stocks.get(i);
                Session.ouvrir(stock);
                StockDAO db = new StockDAO(ChoiceStockActivity.this);
                db.ouvrir();
                db.setStockUtilUse(stock);
                db.fermer();
                Intent intentionEnvoyer = new Intent(ChoiceStockActivity.this,
                        MainActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        View.OnClickListener ajouter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChoiceStockActivity.this,
                        CreateStockActivity.class);
                startActivity(i);
            }
        };
        btnAdd.setOnClickListener(ajouter);
    }


    class ItemStockAdaptateur extends ArrayAdapter<Stock> {

        ItemStockAdaptateur(){
            super(
                    ChoiceStockActivity.this,
                    R.layout.item_stock,
                    R.id.tvNom,
                    stocks
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            return vItem;
        }
    }

}
