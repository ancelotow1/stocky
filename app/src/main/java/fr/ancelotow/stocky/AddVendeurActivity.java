package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class AddVendeurActivity extends AppCompatActivity {

    TextView tvError;
    EditText etNom;
    EditText etTel;
    EditText etEmail;
    EditText etUrl;
    Button btnAjouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vendeur);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        etTel = (EditText) findViewById(R.id.etTel);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etUrl = (EditText) findViewById(R.id.etUrl);
        btnAjouter = (Button) findViewById(R.id.btnAjouter);
        View.OnClickListener ajouter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("")){
                    tvError.setText("Veuillez renseignez le champ NOM.");
                }
                else{
                    Vendeur vendeur = new Vendeur();
                    vendeur.setNom(etNom.getText().toString());
                    vendeur.setTel(etTel.getText().toString());
                    vendeur.setEmail(etEmail.getText().toString());
                    vendeur.setUrl(etUrl.getText().toString());
                    vendeur.setStock(Session.getSession().getStock());
                    VendeurDAO db = new VendeurDAO(AddVendeurActivity.this);
                    db.ouvrir();
                    db.addVendeur(vendeur);
                    db.fermer();
                    Toast.makeText(AddVendeurActivity.this,
                            "Vendeur ajouté avec succès"
                            , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(AddVendeurActivity.this,
                            ListVendeurActivity.class);
                    startActivity(i);
                }
            }
        };
        btnAjouter.setOnClickListener(ajouter);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(AddVendeurActivity.this,
                ListVendeurActivity.class);
        startActivity(i);
        return true;
    }

}
