package fr.ancelotow.stocky.technical;

public enum NomMois {

    Janvier, Février, Mars, Avril, Mai, Juin, Juillet, Août, Septembre, Octobre, Novembre, Décembre

}
