package fr.ancelotow.stocky.technical;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Stock;

public class Session {

    private static Session session = null;
    private Stock stock;
    private boolean tri;
    private boolean action;
    private List<Produit> produits;

    private Session(Stock stock, boolean tri, boolean action, List<Produit> produits){
        super();
        this.stock = stock;
        this.tri = tri;
        this.action = action;
        this.produits = produits;
    }

    public static void ouvrir(Stock stock){
        if(session == null){
            session = new Session(stock, false, false, new ArrayList<Produit>());
        }
    }

    public static Session getSession(){
        return session;
    }

    public Stock getStock(){
        return stock;
    }

    public boolean getTri() {
        return tri;
    }

    public void setTri(boolean tri) {
        this.tri = tri;
    }

    public boolean getAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public void addProduit(Produit produit){
        boolean exist = false;
        for(Produit unP : produits){
            if(unP.getId().equals(produit.getId())){
                exist = true;
                unP.setQuantite(unP.getQuantite() + 1);
            }
        }
        if(!exist){
            produits.add(produit);
        }
    }

    public boolean delProduit(Produit produit){
        boolean exist = false;
        for(int i = 0; i < produits.size(); i++){
            if(produits.get(i).getId().equals(produit.getId())){
                produits.get(i).setQuantite(produits.get(i).getQuantite() - 1);
                exist = true;
                if(produits.get(i).getQuantite() <= 0){
                    produits.remove(i);
                }
            }
        }
        return exist;
    }

    public double prixTotal(){
        double total = 0.00;
        for(Produit unP : produits){
            total = total + (unP.getQuantite()*unP.getPrix());
        }
        return total;
    }

    public void terminerProduit(){
        this.produits = new ArrayList<Produit>();
    }

    public static void fermer(){
        session = null;
    }

}
