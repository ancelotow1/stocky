package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class
ListProduitActivity extends AppCompatActivity {

    ListView lvProduits;
    List<Long> idCats = new ArrayList<Long>();
    List<Produit> produits = new ArrayList<Produit>();
    List<Produit> bonProduit = new ArrayList<Produit>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produit);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvProduits = (ListView) findViewById(R.id.lvProduits);
        CategorieDAO dbCat = new CategorieDAO(this);
        dbCat.ouvrir();
        idCats = dbCat.getIdCategorieStock(Session.getSession().getStock().getId());
        dbCat.fermer();
        ProduitDAO dbProd = new ProduitDAO(this);
        dbProd.ouvrir();
        produits = dbProd.getProduits();
        dbProd.fermer();
        for(Produit unP : produits){
            boolean valid = false;
            for(long idCat : idCats){
                System.out.println("///////////////// " + unP.getNom() + " " +  unP.getCategoerie().getId() + " : " + idCat);
                if(idCat == unP.getCategoerie().getId()){
                    valid = true;
                }
            }
            if(valid){
                bonProduit.add(unP);
            }
        }
        ItemProduitAdaptateur item = new ItemProduitAdaptateur();
        lvProduits.setAdapter(item);
        lvProduits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putString("idProduit", bonProduit.get(i).getId());
                Intent i2 = new Intent(ListProduitActivity.this,
                        ProduitActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ListProduitActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }

    class ItemProduitAdaptateur extends ArrayAdapter<Produit> {
        ItemProduitAdaptateur(){
            super(
                    ListProduitActivity.this,
                    R.layout.item_produit,
                    R.id.tvNom,
                    bonProduit
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            TextView tvQuantite = (TextView) vItem.findViewById(R.id.tvQuantite);
            TextView tvPrix = (TextView) vItem.findViewById(R.id.tvPrix);
            TextView tvVendeur = (TextView) vItem.findViewById(R.id.tvVendeur);
            tvQuantite.setText(Integer.toString(bonProduit.get(position).getQuantite()));
            tvPrix.setText(Double.toString(bonProduit.get(position).getPrix()));
            VendeurDAO dbVend = new VendeurDAO(ListProduitActivity.this);
            dbVend.ouvrir();
            Vendeur vendeur = dbVend.getVendeur(bonProduit.get(position).getVendeur().getId());
            dbVend.fermer();
            tvVendeur.setText(vendeur.getNom());
            if(bonProduit.get(position).getQuantite() <= bonProduit.get(position).getAlert()){
                tvNom.setBackgroundColor(Color.parseColor("#FF9595"));
                tvQuantite.setBackgroundColor(Color.parseColor("#FF9595"));
                tvPrix.setBackgroundColor(Color.parseColor("#FF9595"));
                tvVendeur.setBackgroundColor(Color.parseColor("#FF9595"));
            }
            else{
                tvNom.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvQuantite.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvPrix.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvVendeur.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
            return vItem;
        }
    }

}
