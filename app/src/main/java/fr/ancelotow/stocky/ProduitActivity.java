package fr.ancelotow.stocky;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;

public class ProduitActivity extends AppCompatActivity {

    Produit produit;
    Vendeur vendeur;
    Categorie categorie;
    AlertDialog.Builder alertSup;
    TextView tvNom, tvPrix, tvQuantite, tvAlert;
    Button btnCat, btnVend, btnModif, btnSup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produit);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final String idProduit = pack.getString("idProduit");
        initVar(idProduit);
        tvNom = (TextView) findViewById(R.id.tvNom);
        tvQuantite = (TextView) findViewById(R.id.tvQuantite);
        tvPrix = (TextView) findViewById(R.id.tvPrix);
        tvAlert = (TextView) findViewById(R.id.tvSeuilAlert);
        btnCat = (Button) findViewById(R.id.btnCat);
        btnVend = (Button) findViewById(R.id.btnVend);
        btnModif = (Button) findViewById(R.id.btnModifier);
        btnSup = (Button) findViewById(R.id.btnSupprimer);
        tvNom.setText(produit.getNom());
        tvQuantite.setText(Integer.toString(produit.getQuantite()));
        tvAlert.setText(Integer.toString(produit.getAlert()));
        tvPrix.setText(Double.toString(produit.getPrix()));
        btnCat.setText(categorie.getNom());
        btnVend.setText(vendeur.getNom());
        alertSup = new AlertDialog.Builder(this);
        alertSup.setPositiveButton(R.string.btnOuiSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                ProduitDAO dbProd = new ProduitDAO(ProduitActivity.this);
                dbProd.ouvrir();
                dbProd.supProduit(produit);
                dbProd.fermer();
                Toast.makeText(ProduitActivity.this,
                        "Produit supprimé avec succès."
                        , Toast.LENGTH_LONG).show();
                Bundle pack = new Bundle();
                pack.putLong("idCategorie", produit.getCategoerie().getId());
                Intent i2 = new Intent(ProduitActivity.this,
                        CategorieActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
        alertSup.setNegativeButton(R.string.btnNonSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        View.OnClickListener goCat = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle pack = new Bundle();
                pack.putLong("idCategorie", produit.getCategoerie().getId());
                Intent i2 = new Intent(ProduitActivity.this,
                        CategorieActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        };
        View.OnClickListener goVend = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle pack = new Bundle();
                pack.putLong("idVendeur", produit.getVendeur().getId());
                Intent i2 = new Intent(ProduitActivity.this,
                        VendeurActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        };
        View.OnClickListener modifier = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle pack = new Bundle();
                pack.putString("idProduit", produit.getId());
                Intent i2 = new Intent(ProduitActivity.this,
                        ModifProduitActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        };
        View.OnClickListener supprimer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertSup.show();
            }
        };
        alertSup.setMessage(R.string.tvSupProduit);
        btnCat.setOnClickListener(goCat);
        btnVend.setOnClickListener(goVend);
        btnModif.setOnClickListener(modifier);
        btnSup.setOnClickListener(supprimer);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ProduitActivity.this,
                ListProduitActivity.class);
        startActivity(i);
        return true;
    }

    public void initVar(String idProd){
        ProduitDAO dbProd = new ProduitDAO(this);
        dbProd.ouvrir();
        produit = dbProd.getProduit(idProd);
        dbProd.fermer();
        VendeurDAO dbVend = new VendeurDAO(this);
        dbVend.ouvrir();
        vendeur = dbVend.getVendeur(produit.getVendeur().getId());
        dbVend.fermer();
        CategorieDAO dbCat = new CategorieDAO(this);
        dbCat.ouvrir();
        categorie = dbCat.getCategorie(produit.getCategoerie().getId());
        dbCat.fermer();
    }

}
