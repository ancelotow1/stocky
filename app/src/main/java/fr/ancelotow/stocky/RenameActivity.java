package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.technical.Session;

public class RenameActivity extends AppCompatActivity {

    EditText etNom;
    TextView tvError;
    Button btnRename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rename);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(Session.getSession().getStock().getNom());
        etNom = (EditText) findViewById(R.id.etNom);
        tvError = (TextView) findViewById(R.id.tvError);
        btnRename = (Button) findViewById(R.id.btnRenommer);
        View.OnClickListener renommer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("")){
                    tvError.setText("Le champ nom est obligatoire.");
                }
                else{
                    Session.getSession().getStock().setNom(etNom.getText().toString());
                    StockDAO db = new StockDAO(RenameActivity.this);
                    db.ouvrir();
                    db.setStockName(Session.getSession().getStock());
                    db.fermer();
                    Toast.makeText(RenameActivity.this,
                            "Le stock à été renommer avec succès."
                            , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(RenameActivity.this,
                            MainActivity.class);
                    startActivity(i);
                }
            }
        };
        btnRename.setOnClickListener(renommer);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(RenameActivity.this,
                ParametreActivity.class);
        startActivity(i);
        return true;
    }

}
