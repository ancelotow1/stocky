package fr.ancelotow.stocky;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;

public class CategorieActivity extends AppCompatActivity {

    ListView lvProduits;
    Categorie categorie;
    AlertDialog.Builder alertSup;
    List<Produit> produits = new ArrayList<Produit>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorie);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        final long idCategorie = pack.getLong("idCategorie");
        CategorieDAO db = new CategorieDAO(this);
        db.ouvrir();
        categorie = db.getCategorie(idCategorie);
        db.fermer();
        setTitle(categorie.getNom().toUpperCase());
        alertSup = new AlertDialog.Builder(this);
        alertSup.setPositiveButton(R.string.btnOuiSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                ProduitDAO dbProd = new ProduitDAO(CategorieActivity.this);
                dbProd.ouvrir();
                dbProd.supProduitCat(categorie.getId());
                dbProd.fermer();
                CategorieDAO db = new CategorieDAO(CategorieActivity.this);
                db.ouvrir();
                db.supCategorie(categorie);
                db.fermer();
                File dfile = new File(categorie.getImage());
                dfile.delete();
                Toast.makeText(CategorieActivity.this,
                        "Catégorie supprimé avec succès."
                        , Toast.LENGTH_LONG).show();
                Intent intentionEnvoyer = new Intent(CategorieActivity.this,
                        ListCategorieActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        alertSup.setNegativeButton(R.string.btnNonSup, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertSup.setMessage(R.string.tvSupCategorie);
        lvProduits = (ListView) findViewById(R.id.lvProduits);
        ProduitDAO dbProd = new ProduitDAO(this);
        dbProd.ouvrir();
        produits = dbProd.getProduitsCat(categorie.getId());
        dbProd.fermer();
        ItemProduitAdaptateur item = new ItemProduitAdaptateur();
        lvProduits.setAdapter(item);
        lvProduits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putString("idProduit", produits.get(i).getId());
                Intent i2 = new Intent(CategorieActivity.this,
                        ProduitActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(CategorieActivity.this,
                ListCategorieActivity.class);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_categorie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       if(item.getItemId() == R.id.menuAjouter ) {
           Toast.makeText(CategorieActivity.this,
                   "Section en cours de développement."
                   , Toast.LENGTH_LONG).show();
       }
       else if(item.getItemId() == R.id.menuModifier){
                Bundle pack = new Bundle();
                pack.putLong("idCategorie", categorie.getId());
                Intent i2 = new Intent(CategorieActivity.this,
                        ModifCategorieActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
        }
       else if(item.getItemId() == R.id.menuSupprimer){
                alertSup.show();
        }
        return super.onOptionsItemSelected(item);
    }

    class ItemProduitAdaptateur extends ArrayAdapter<Produit> {
        ItemProduitAdaptateur(){
            super(
                    CategorieActivity.this,
                    R.layout.item_produit,
                    R.id.tvNom,
                    produits
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            TextView tvQuantite = (TextView) vItem.findViewById(R.id.tvQuantite);
            TextView tvPrix = (TextView) vItem.findViewById(R.id.tvPrix);
            TextView tvVendeur = (TextView) vItem.findViewById(R.id.tvVendeur);
            tvQuantite.setText(Integer.toString(produits.get(position).getQuantite()));
            tvPrix.setText(Double.toString(produits.get(position).getPrix()));
            VendeurDAO dbVend = new VendeurDAO(CategorieActivity.this);
            dbVend.ouvrir();
            Vendeur vendeur = dbVend.getVendeur(produits.get(position).getVendeur().getId());
            dbVend.fermer();
            tvVendeur.setText(vendeur.getNom());
            if(produits.get(position).getQuantite() <= produits.get(position).getAlert()){
                tvNom.setBackgroundColor(Color.parseColor("#FF9595"));
                tvQuantite.setBackgroundColor(Color.parseColor("#FF9595"));
                tvPrix.setBackgroundColor(Color.parseColor("#FF9595"));
                tvVendeur.setBackgroundColor(Color.parseColor("#FF9595"));
            }
            else{
                tvNom.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvQuantite.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvPrix.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvVendeur.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }
            return vItem;
        }
    }

}
