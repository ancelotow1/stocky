package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.FactureDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Facture;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class BudgetActivity extends AppCompatActivity {

    ListView lvFactures;
    List<Facture> factures = new ArrayList<Facture>();
    final DecimalFormat df = new DecimalFormat("0.000");

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Budget : " +
                String.valueOf(df.format(Session.getSession().getStock().getBudget())) + " €");
        lvFactures = (ListView) findViewById(R.id.lvFactures);
        FactureDAO db = new FactureDAO(this);
        db.ouvrir();
        try {
            factures = db.getFactureStock(Session.getSession().getStock().getId());
            db.fermer();
        } catch (ParseException e) {
            e.printStackTrace();
            db.fermer();
        }
        ItemFactureAdaptateur item = new ItemFactureAdaptateur();
        lvFactures.setAdapter(item);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(BudgetActivity.this,
                MainActivity.class);
        startActivity(i);
        return true;
    }


    class ItemFactureAdaptateur extends ArrayAdapter<Facture> {
        ItemFactureAdaptateur(){
            super(
                    BudgetActivity.this,
                    R.layout.item_facture,
                    R.id.tvDate,
                    factures
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvPrix = (TextView) vItem.findViewById(R.id.tvPrix);
            tvPrix.setText(Double.toString(factures.get(position).getMontant()) + " €");
            return vItem;
        }
    }

}
