package fr.ancelotow.stocky;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.database.ProduitDAO;
import fr.ancelotow.stocky.database.VendeurDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.entity.Produit;
import fr.ancelotow.stocky.entity.Vendeur;
import fr.ancelotow.stocky.technical.Session;

public class ListCourseActivity extends AppCompatActivity {

    ListView lvProduits;
    TextView tvType;
    List<Produit> produits = new ArrayList<Produit>();
    boolean type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_course);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle pack = this.getIntent().getExtras();
        lvProduits = (ListView) findViewById(R.id.lvProduits);
        tvType = (TextView) findViewById(R.id.tvType);
        final long id = pack.getLong("id");
        type = Session.getSession().getTri();
        ProduitDAO dbProd = new ProduitDAO(this);
        dbProd.ouvrir();
        if(!type){
            produits = dbProd.getRupProduitsVend(id);
            tvType.setText("CATEGORIE");
        }
        else{
            produits = dbProd.getRupProduitsCat(id);
            tvType.setText("VENDEUR");
        }
        dbProd.fermer();
        ItemProduitAdaptateur item = new ItemProduitAdaptateur();
        lvProduits.setAdapter(item);
        lvProduits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle pack = new Bundle();
                pack.putString("idProduit", produits.get(i).getId());
                Intent i2 = new Intent(ListCourseActivity.this,
                        ProduitActivity.class);
                i2.putExtras(pack);
                startActivity(i2);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(ListCourseActivity.this,
                CourseActivity.class);
        startActivity(i);
        return true;
    }

    class ItemProduitAdaptateur extends ArrayAdapter<Produit> {
        ItemProduitAdaptateur(){
            super(
                    ListCourseActivity.this,
                    R.layout.item_produit,
                    R.id.tvNom,
                    produits
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            TextView tvQuantite = (TextView) vItem.findViewById(R.id.tvQuantite);
            TextView tvPrix = (TextView) vItem.findViewById(R.id.tvPrix);
            TextView tvType = (TextView) vItem.findViewById(R.id.tvVendeur);
            tvQuantite.setText(Integer.toString(produits.get(position).getQuantite()));
            tvPrix.setText(Double.toString(produits.get(position).getPrix()));
            String nom;
            if(type){
                VendeurDAO dbVend = new VendeurDAO(ListCourseActivity.this);
                dbVend.ouvrir();
                Vendeur vendeur = dbVend.getVendeur(produits.get(position).getVendeur().getId());
                dbVend.fermer();
                nom = vendeur.getNom();
            }
            else{
                CategorieDAO dbCat = new CategorieDAO(ListCourseActivity.this);
                dbCat.ouvrir();
                Categorie categorie =
                        dbCat.getCategorie(produits.get(position).getCategoerie().getId());
                dbCat.fermer();
                nom = categorie.getNom();
            }
            tvType.setText(nom);
            return vItem;
        }
    }

}
