package fr.ancelotow.stocky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.ancelotow.stocky.database.StockDAO;
import fr.ancelotow.stocky.technical.Session;
import fr.ancelotow.stocky.entity.Stock;

public class CreateStockActivity extends AppCompatActivity {

    Button btnCreate;
    EditText etNom;
    EditText etBudget;
    TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_stock);
        final Stock stock = new Stock();
        etNom = (EditText) findViewById(R.id.etNom);
        etBudget = (EditText) findViewById(R.id.etBudget);
        tvError = (TextView) findViewById(R.id.tvError);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        View.OnClickListener creer = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("") ||
                        etBudget.getText().toString().equals("")){
                    tvError.setText("Vous devez renseigner tout les champs.");
                }
                else{
                    stock.setNom(etNom.getText().toString());
                    stock.setBudget(Double.parseDouble(etBudget.getText().toString()));
                    StockDAO db = new StockDAO(CreateStockActivity.this);
                    db.ouvrir();
                    long id = db.addStock(stock);
                    db.fermer();
                    if(id < 0 ){
                        Toast.makeText(CreateStockActivity.this,
                                "Une erreur est survenu lors de la création du stock"
                                , Toast.LENGTH_LONG).show();
                    } else{
                        stock.setId(id);
                        Session.ouvrir(stock);
                        Intent i = new Intent(CreateStockActivity.this,
                                MainActivity.class);
                        startActivity(i);
                    }
                }

            }
        };
        btnCreate.setOnClickListener(creer);
    }

}
