package fr.ancelotow.stocky;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;

import fr.ancelotow.stocky.database.CategorieDAO;
import fr.ancelotow.stocky.entity.Categorie;
import fr.ancelotow.stocky.technical.Session;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class AddCategorieActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    final static int SELECT_PICTURE = 1;
    TextView tvError;
    EditText etNom;
    ImageView ivImg;
    Button btnImg, btnAdd;
    Bitmap bitmap = null;
    Categorie categorie;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_categorie);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        categorie = new Categorie();
        tvError = (TextView) findViewById(R.id.tvError);
        etNom = (EditText) findViewById(R.id.etNom);
        btnImg = (Button) findViewById(R.id.btnImg);
        btnAdd = (Button) findViewById(R.id.btnAjouter);
        ivImg = (ImageView) findViewById(R.id.ivImg);
        View.OnClickListener image = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                requestRead(intent);
            }
        };
        View.OnClickListener ajouter = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNom.getText().toString().equals("")){
                    tvError.setText("Le champ NOM doit être rempli.");
                }
                else{
                    init(AddCategorieActivity.this);
                    if(bitmap == null){
                        categorie.setImage("aucune");
                    }
                    else{
                        saveImg();
                    }
                    categorie.setNom(etNom.getText().toString());
                    categorie.setStock(Session.getSession().getStock());
                    CategorieDAO db = new CategorieDAO(AddCategorieActivity.this);
                    db.ouvrir();
                    db.addCategorie(categorie);
                    db.fermer();
                    Toast.makeText(AddCategorieActivity.this,
                            "Catégorie ajouté avec succès"
                            , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(AddCategorieActivity.this,
                            ListCategorieActivity.class);
                    startActivity(i);
                }
            }
        };
        btnImg.setOnClickListener(image);
        btnAdd.setOnClickListener(ajouter);
    }

    public void requestRead(Intent intent) {
        if (ContextCompat.checkSelfPermission(this,
                READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
        }
    }

    public void requestWrite() {
        if (ContextCompat.checkSelfPermission(this,
                WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        } else {
            saveImg();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent i = new Intent(AddCategorieActivity.this,
                ListCategorieActivity.class);
        startActivity(i);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ivImg = (ImageView) findViewById(R.id.ivImg);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_PICTURE:
                    String path = getRealPathFromURI(this, data.getData());
                    Log.d("Choose Picture", path);
                    bitmap = BitmapFactory.decodeFile(path);
                    ivImg.setImageBitmap(bitmap);
                    break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getRealPathFromURI(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
            }
            else {
                Toast.makeText(AddCategorieActivity.this,
                        "Permission refusé.", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        else{
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveImg();
            }
            else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void init(Context context) {
        path = context.getExternalFilesDir(null).getAbsolutePath();
    }

    public void saveImg(){
        try {
            FileOutputStream output = null;
            File file = new File(path,
                    etNom.getText().toString().replaceAll("\\s", "") + "_" +
                    Session.getSession().getStock().getNom().replaceAll("\\s", "") +
                    Session.getSession().getStock().getId() + ".png");
            output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, output);
            categorie.setImage(file.getAbsolutePath());
            output.flush();
            output.close();
            MediaStore.Images.Media.insertImage(getContentResolver(),
                    file.getAbsolutePath(),file.getName(),file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
